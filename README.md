# Desctiption
Transparent pillars - Don't Starve Together modification.

Sets transparency pillars for a comfortable view. Any objects are clickable through pillars.
You can adjust transparency level in options.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1288937992)
