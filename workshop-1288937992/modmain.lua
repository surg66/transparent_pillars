--prefab | alpha | no click
local table_prefabs =
{
    ["cave_exit"]                      = { GetModConfigData("ALPHA_CAVE_EXIT"),             false },
    ["tentacle_pillar"]                = { GetModConfigData("ALPHA_TENTACLE_PILLAR"),       false },
    ["pillar_algae"]                   = { GetModConfigData("ALPHA_PILLAR_ALGAE"),          true  },
    ["pillar_atrium"]                  = { GetModConfigData("ALPHA_PILLAR_ATRIUM"),         true  },
    ["pillar_cave"]                    = { GetModConfigData("ALPHA_PILLAR_CAVE"),           true  },
    ["pillar_cave_flintless"]          = { GetModConfigData("ALPHA_PILLAR_CAVE_FLINTLESS"), true  },
    ["pillar_cave_rock"]               = { GetModConfigData("ALPHA_PILLAR_CAVE_ROCK"),      true  },
    ["pillar_stalactite"]              = { GetModConfigData("ALPHA_PILLAR_STALACTITE"),     true  },
    ["pillar_ruins"]                   = { GetModConfigData("ALPHA_PILLAR_RUINS"),          true  },
    ["ruins_cavein_obstacle"]          = { GetModConfigData("ALPHA_PILLAR_RUINS"),          false },
    ["daywalker_pillar"]               = { GetModConfigData("ALPHA_DAYWALKER_PILLAR"),      false }, -- Cracked Pillar
    ["support_pillar"]                 = { GetModConfigData("ALPHA_PILLAR_SUPPORT"),        false },
    ["support_pillar_scaffold"]        = { GetModConfigData("ALPHA_PILLAR_SUPPORT"),        false },
    ["support_pillar_scaffold_placer"] = { GetModConfigData("ALPHA_PILLAR_SUPPORT"),        false },
    ["support_pillar_dreadstone"]                 = { GetModConfigData("ALPHA_PILLAR_SUPPORT_DREADSTONE"), false },
    ["support_pillar_dreadstone_scaffold"]        = { GetModConfigData("ALPHA_PILLAR_SUPPORT_DREADSTONE"), false },
    ["support_pillar_dreadstone_scaffold_placer"] = { GetModConfigData("ALPHA_PILLAR_SUPPORT_DREADSTONE"), false },
    ["archive_pillar"]               = { GetModConfigData("ALPHA_ARCHIVE_PILLAR"),      true  },
    ["archive_chandelier"]           = { GetModConfigData("ALPHA_ARCHIVE_CHANDELIER"),  true  },
    ["chandelier_fire"]              = { GetModConfigData("ALPHA_ARCHIVE_CHANDELIER"),  true  },
    ["grotto_moonglass_1"]           = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["grotto_moonglass_3"]           = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["grotto_moonglass_4"]           = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["grotto_waterfall_big"]         = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["grotto_waterfall_small1"]      = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["grotto_waterfall_small2"]      = { GetModConfigData("ALPHA_VITREOASIS"),          false },
    ["mast"]                         = { GetModConfigData("ALPHA_MAST"),                false },
    ["mast_malbatross"]              = { GetModConfigData("ALPHA_MAST_MALBATROSS"),     false },
    ["mastupgrade_lamp"]             = { GetModConfigData("ALPHA_MASTUPGRADE"),         true  },
    ["mastupgrade_lightningrod"]     = { GetModConfigData("ALPHA_MASTUPGRADE"),         true  },
    ["mastupgrade_lightningrod_top"] = { GetModConfigData("ALPHA_MASTUPGRADE"),         true  },
    ["monkeypillar"]                 = { GetModConfigData("ALPHA_MONKEY_PILLAR"),       true  },
    ["oceanvine"]                    = { GetModConfigData("ALPHA_OCEANVINE"),           false }, -- Mossy Vine
    ["oceanvine_deco"]               = { GetModConfigData("ALPHA_OCEANVINE_DECO"),      true  }, 
    ["oceanvine_cocoon"]             = { GetModConfigData("ALPHA_OCEANVINE_COCOON"),    false },
    ["oceanvine_cocoon_burnt"]       = { GetModConfigData("ALPHA_OCEANVINE_COCOON"),    false },
    ["oceantree"]                    = { GetModConfigData("ALPHA_OCEANTREE"),           false }, -- Knobbly tree
    ["oceantree_pillar"]             = { GetModConfigData("ALPHA_OCEANTREE_PILLAR"),    true  }, -- Above-Average Tree Trunk
    ["watertree_pillar"]             = { GetModConfigData("ALPHA_OCEANTREE_PILLAR"),    true  }, -- Great Tree Trunk
    ["evergreen"]                    = { GetModConfigData("ALPHA_EVERGREEN"),           false }, -- Evergreen
    ["evergreen_sparse"]             = { GetModConfigData("ALPHA_EVERGREEN_SPARSE"),    false }, -- Lumpy Evergreen
    ["deciduoustree"]                = { GetModConfigData("ALPHA_DECIDUOUSTREE"),       false }, -- Birchnut Tree
    ["rock_petrified_tree"]          = { GetModConfigData("ALPHA_ROCK_PETRIFIED_TREE"), false }, -- Petrified Tree
    ["moon_tree"]                    = { GetModConfigData("ALPHA_MOON_TREE"),           false }, -- Lune Tree
    ["sandspike_short"]              = { GetModConfigData("ALPHA_SANDSPIKE"),           false }, -- Sand Spike
    ["sandspike_med"]                = { GetModConfigData("ALPHA_SANDSPIKE"),           false }, -- Sand Spike
    ["sandspike_tall"]               = { GetModConfigData("ALPHA_SANDSPIKE"),           false }, -- Sand Spike
    ["sandblock"]                    = { GetModConfigData("ALPHA_SANDBLOCK"),           false }, -- Sand Castle
    ["pighouse"]                     = { GetModConfigData("ALPHA_PIGHOUSE"),            false }, -- Pig House
    ["rabbithouse"]                  = { GetModConfigData("ALPHA_RABBITHOUSE"),         false }, -- Rabbit Hutch
    ["mermhouse"]                    = { GetModConfigData("ALPHA_MERMHOUSE"),           false }, -- Leaky Shack
    ["mermhouse_crafted"]            = { GetModConfigData("ALPHA_MERMHOUSE_CRAFTED"),   false }, -- Craftsmerm House
    ["mermwatchtower"]               = { GetModConfigData("ALPHA_MERMWATCHTOWER"),      false }, -- Merm Flort-ifications
}

local alpha_canopy = GetModConfigData("ALPHA_CANOPY_TRANSPARENCY")

for prefab, data in pairs(table_prefabs) do
    local alpha = data[1]
    if alpha < 1 then
        AddPrefabPostInit(prefab, function(inst)
            if inst.prefab == "tentacle_pillar" then
                --support for mod "Wormhole Icons [Fixed]"
                inst:DoTaskInTime(0.3, function()
                    if inst.border_circle ~= nil then
                        local r, g, b, a = inst.border_circle.AnimState:GetAddColour()
                        inst.AnimState:OverrideMultColour(r, g, b, alpha)
                    else
                        inst.AnimState:OverrideMultColour(alpha, alpha, alpha, alpha)
                    end
                end)
            else
                inst.AnimState:OverrideMultColour(alpha, alpha, alpha, alpha)
            end

            --if "no click"
            if data[2] == true then
                inst:AddTag("NOCLICK")
                inst.CanMouseThrough = function(inst) return true, true end
            end
        end)
    end
end

if alpha_canopy < 1 then
    AddClassPostConstruct("widgets/leafcanopy", function(self)
        for row = 1, 5 do
            for i = 1, 5 do
                self["leavesTop"..row.."_"..i]:GetAnimState():OverrideMultColour(alpha_canopy, alpha_canopy, alpha_canopy, alpha_canopy)
            end
        end
    end)
end
